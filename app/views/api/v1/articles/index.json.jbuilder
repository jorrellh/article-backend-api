json.data do
    json.array! @articles do |article|
        json.partial! 'api/v1/articles/article', article: article
    end
end