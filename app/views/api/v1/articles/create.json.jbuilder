json.data do
    json.user do
        json.partial! 'api/v1/articles/article', article: @article
    end
end