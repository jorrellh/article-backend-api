module Api
    module V1
        class ArticlesController < ApplicationController
            def index
                #@articles = current_user&.articles
                @articles = User.first.articles
                render :index, status: :ok
            end

            def show
                @article = current_user&.articles.find(params[:id])
                render :show, status: :ok
            end

            def create
                @article = current_user.articles.build(article_params)
                if @article.save
                    render :create, status: :created
                else
                    head(:unprocessable_entity)
                end

            end

            def destroy
                @article = current_user.articles.where(id: params[:id])
                if @article.destroy
                    head(:ok)
                else
                    head(:unprocessable_entity) 
                end
            end

            def update
                @article = Article.find(params[:id])
                if @article.update_attributes(article_params)
                    render json: {message: 'Updated article' }, status: :ok
                else
                    head(:unprocessable_entity)
                end
            end

            private

            def article_params
                params.require(:article).permit(:title, :body)
            end

        end
    end
end